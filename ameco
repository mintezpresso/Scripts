#!/bin/sh
# Adaptive play/pause toggle for mpd and mpv
# mintezpresso

#################### Declare all vars and helper funcs ####################

# Config file. Yes this thing now needs a config file
. $HOME/.config/ameco/config

# SPOTIFYD varts
# if spotifyd is on, it REPLACES mpd as the default music player

fn_start_spot () { setsid spotifyd; pamixer --set-volume 30 ;}

spot=$(dbus-send --session --dest=org.freedesktop.DBus --type=method_call --print-reply /org/freedesktop/DBus org.freedesktop.DBus.ListNames | grep -wo spotifyd.in.* | tr -d '\"')

# TODO: if $spot err then kill the process that spawn later, keep only the oldest one

[ -z "$spot" ] && killall spotifyd && fn_start_spot

sptpid=$(pgrep -x spotifyd)
sptstate=$(dbus-send --print-reply --dest=org.mpris.MediaPlayer2."$spot" /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:"org.mpris.MediaPlayer2.Player" string:'PlaybackStatus' | grep -Ev "^method" | grep -Eo '("(.*)")|(\b[0-9][a-zA-Z0-9.]*\b)' | cut -f2 -d'"')

spttoggle () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2.$spot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause > /dev/null;}

sptplay () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2.$spot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play > /dev/null;}

sptpause () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2.$spot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause > /dev/null;}

sptnext () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2.$spot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next ;}

sptprev () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2.$spot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous ;}

sptfwd () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2."$spot" /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Seek int64:"$seekvalue"000000 >/dev/null 2>&1 ;}

sptrwd () { dbus-send --print-reply --dest=org.mpris.MediaPlayer2."$spot" /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Seek int64:"-$seekvalue"000000 >/dev/null 2>&1 ;}

# MPV vars
mpvtoggle () { echo "cycle pause" | socat - "/tmp/mpvsocket" ;}
mpvpause () { echo '{ "command": ["set_property", "pause", true] }' | socat - /tmp/mpvsocket ;}

# next-prev vars
mpvnext () { echo playlist-next | socat - "/tmp/mpvsocket" ;}
mpvfwd () { echo "seek $seekvalue" | socat - "/tmp/mpvsocket" ;}

mpvprev () { echo playlist-prev | socat - "/tmp/mpvsocket"; }
mpvrwd () { echo "seek -$seekvalue" | socat - "/tmp/mpvsocket" ;}

# global vars and funcs
mpvpid=$(pgrep -x mpv)
mpdpid=$(pgrep -x mpd)
mpdstate=$(mpc status %state% 2>/dev/null)
MPVIDLE=$(echo '{ "command": ["get_property", "core-idle"] }' | socat - /tmp/mpvsocket | jq .data | sed -e 's/"//g;s/ //g' )

# get name of whatever is being used with fn_play
# made for mpv and dwmblocks

NAMEFILE="$HOME/.cache/statusbar/mpv/targetname"
VOLFILE="$HOME/.cache/statusbar/mpv/volvar"

refresh_block () {
    cat /dev/null > $NAMEFILE
    pkill -RTMIN+$VOLSIG dwmblocks
}

refresh_mpv_module () {
    pkill -RTMIN+$MPVSIG dwmblocks
    rm /tmp/mpvsocket
}

get_target_name () {
    mkdir -p "$HOME/.cache/mpv"

    case "$target" in
        *watch* )
            targetname=$(yt-dlp --get-filename $target | sed "s/ *\[.*\]//g;s/\ /_/g;s/\.webm//g")
            [ ! "$(cat $NAMEFILE)" = "$targetname" ] && rm $NAMEFILE
            ;;

        *) targetname="$(basename $target)"
    esac

    printf "🎬 $targetname" | tr '_' ' ' > $NAMEFILE
    refresh_mpv_module
    # change this to your dwmblocks sig
}

##################################################################################################

# just a noti. sent when next-prev-new song
fn_mpd_noti () {
    dunstify -t $play_duration "🟡 Now Playing" "Name: <b>$(mpc --format '%title%' current)</b>\nArtist: <b>$(mpc --format %artist% current)</b>"
}

fn_seek_fw () {
    case $sptpid in
        "" )
        case $mpdpid in
            "" ) mpvfwd ;; # mpd off, mpv on
            * ) case $mpvpid in # mpd on
                "" ) mpc seek +$seekvalue ;; # mpd on , mpv off
                * ) case $mpdstate in # mpd on, mpv on
                       "paused" ) [ "$MPVIDLE" = "false" ] && mpvfwd ;; # mpd paused, mpv playing
                        * ) [ "$MPVIDLE" = "false" ] && mpc pause && mpvfwd # mpd playing, mpv playing
                            [ "$MPVIDLE" = "true" ] && mpc seek +$seekvalue ;; # mpd playing, mpv paused
                    esac
            esac
        esac ;;
        * )  sptfwd
    esac
}

fn_seek_bw () {
    case $sptpid in
        "" )
        case $mpdpid in
            "" ) mpvrwd ;; # mpd off, mpv on
            *) case $mpvpid in # mpd on
                "" ) mpc seek -$seekvalue ;; # mpd on , mpv off
                *) case $mpdstat in # mpd on, mpv on
                       "paused" ) [ "$MPVIDLE" = "false" ] && mpvprev ;; # mpd paused, mpv playing
                        * ) [ "$MPVIDLE" = "false" ] && mpc pause && mpvprev # mpd playing, mpv playing
                            [ "$MPVIDLE" = "true" ] && mpc seek -$seekvalue ;; # mpd playing, mpv paused
                    esac
            esac
        esac ;;
        * ) sptrwd
    esac
}

fn_prev () {
    case $sptpid in
        "" )
        case $mpdpid in
            "" ) mpvprev ;;
            * )
                case $mpvpid in
                "" ) mpc prev && fn_mpd_noti ;;
                * )
                    case $mpdstate in
                       "paused" ) [ "$MPVIDLE" = "false" ] && mpvprev ;;
                        * ) [ "$MPVIDLE" = "false" ] && mpc pause && mpvprev
                        [ "$MPVIDLE" = "true" ] && mpc prev && fn_mpd_noti
                    esac
                esac
        esac ;;
        * ) sptprev
    esac
}

fn_next () {
    case $sptpid in
        "" )
            case $mpdpid in
            "" ) mpvnext ;;
            * )
                case $mpvpid in
                "" ) mpc next && fn_mpd_noti ;;
                * )
                    case $mpdstate in
                    "paused" ) [ "$MPVIDLE" = "false" ] && mpvnext ;;
                    * ) [ "$MPVIDLE" = "false" ] && mpc pause && mpvnext
                        [ "$MPVIDLE" = "true" ] && mpc next && fn_mpd_noti
                    esac
                esac
            esac;;

        * ) sptnext
    esac
}

##################################################################################################

# use this in ~/.xinitrc
fn_start () { mpd && sb-mpdup ;}

# has to be && cuz spotifyd will mess up volume level after successful launch

# self explanatory
fn_player () {
    case $sptpid in
        "" ) wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.3; ${editor:-$TERMINAL} ncmpcpp ;;
        * ) ${editor:-$TERMINAL} spt ;;
    esac
}

# I imagined single-shuffle-repeat as switches when writing this
fn_switch () {
    getswitch=$(printf "Single\nShuffle\nRepeat" | dmenu -i -p "mpc actions")

    [ -z "$getswitch" ] && exit 1

    case "$getswitch" in

        "Single" ) [ "$(mpc status %single%)" = "on" ] \
            && (mpc single off && dunstify -t $switch_duration "Single off" && exit 0 ) \
            || (mpc single on && dunstify -t $switch_duration "Single on" && exit 0 )
        ;;

        "Shuffle" ) [ "$(mpc status %random%)" = "on" ] \
            && (mpc random off && dunstify -t $switch_duration "Shuffle off" && exit 0 ) \
            || (mpc random on && dunstify -t $switch_duration "Shuffle on" && exit 0 )
        ;;

        "Repeat" ) [ "$(mpc status %repeat%)" = "on" ] \
            && (mpc repeat off && dunstify -t $switch_duration "Repeat off" && exit 0 ) \
            || (mpc repeat on && dunstify -t $switch_duration "Repeat on" && exit 0 )
        ;;

        * ) dunstify "Invalid option" && exit 1
    esac
}

##################################################################################################

# Toggle commands
# They make ameco adaptive

getvol () {
    pamixer --get-volume > $VOLFILE
}

revol () {
    pamixer --set-volume $(cat $VOLFILE)
}

revol_mpd () {
    pamixer --set-volume $mpd_default_vol
}

# both on, but only mpd playing
check_then_toggle () {
    case "$lastplayed" in
        mpd ) mpc toggle && lastplayed="mpd" ;; # can just say play here cuz we know it was previously paused
        mpv | "" ) mpvtoggle && lastplayed="mpv" ;; # same with this one
    esac
}

toggleboth () {

    case $lastplayed in
        mpv | "" ) getvol && revol_mpd && lastplayed="mpd" ;;
        mpd ) revol && lastplayed="mpv"
    esac

    mpc toggle & mpvtoggle
    refresh_block
}

fn_allpause () {

    # both on
    # toggle-m will priorities mpv, assuming user usually only has mpd on and will use mpv only when needed to play video and want music off during

    [ -n "$mpdpid" ] && [ -n "$mpvpid" ] && [ "$mpdstate" = "playing" ] && [ "$MPVIDLE" = "true" ] && lastplayed="mpd" # only mpd playing
    [ -n "$mpdpid" ] && [ -n "$mpvpid" ] && [ "$mpdstate" = "playing" ] && [ "$MPVIDLE" = "false" ] && lastplayed="mpv" # both playing
    [ -n "$mpdpid" ] && [ -n "$mpvpid" ] && [ "$mpdstate" = "paused" ] && [ "$MPVIDLE" = "true" ] && lastplayed="mpv" # both paused
    [ -n "$mpdpid" ] && [ -n "$mpvpid" ] && [ "$mpdstate" = "paused" ] && [ "$MPVIDLE" = "false" ] && lastplayed="mpv" # only mpv playing

    [ -n "$mpdpid" ] && [ -z "$mpvpid" ] && lastplayed="mpd" # mpd on only
    [ -z "$mpdpid" ] && [ -n "$mpvpid" ] && lastplayed="mpv" # mpv on only

    # here comes firefox
    # FIREFOX vars. full support not ready yet. this is only for fn_allpause

    fox=$(dbus-send --session --dest=org.freedesktop.DBus --type=method_call --print-reply /org/freedesktop/DBus org.freedesktop.DBus.ListNames | grep -wo firefox.in.* | tr -d '\"')

    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.$fox /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause >/dev/null 2>&1
    # we call this foxpause later

    mpvpause
    mpc pause
    sptpause
}

fn_toggle () {

    case $sptpid in
        "" )
            case $mpdpid in
                "" ) mpvtoggle && lastplayed="mpv" ;; # mpd off
                * )
                    case $mpvpid in # mpd on
                        "" ) mpc toggle && lastplayed="mpd" ;; # mpd on, mpv off
                        * ) [ "$mpdstate" = "paused" ] && [ "$MPVIDLE" = "true" ] && check_then_toggle # both on and paused. this is the situation where you're mainly watching mpv with mpd on pause
                            [ "$mpdstate" = "playing" ] && [ "$MPVIDLE" = "true" ] && lastplayed="mpd" && toggleboth # both on but 1 paused
                            [ "$mpdstate" = "paused" ] && [ "$MPVIDLE" = "false" ] && lastplayed="mpv" && toggleboth # both on but 1 paused
                    esac
            esac ;;
        * ) spttoggle
    esac
# resume system:
# check for last played, then if both on and paused, play last played
# originally designed for all pause, should work with manual pause

}

##################################################################################################

fn_select () {
     track="$(mpc playlist -f "%position% %title% - %artist%" | dmenu -i -l 5 -g 8 -p "Pick a track")"
     trackname="$(printf '%s' "${track%% *}")"
     name_number=$(echo ${track%%'-'*})

     name=$(echo ${name_number#*' '})
     artist=$(echo ${track#*'-'*})

     # https://www.howtogeek.com/812494/bash-string-manipulation/

     [ -n "$trackname" ] \
         && dunstify -t 2000 "🟡 Now Playing" "Name: <b>$name</b>\nArtist: <b>$artist</b>" \
         || exit

     # error message
     [ "$(pgrep -x mpd)" ] && checkplayer="check pipewire" || checkplayer="mpd not running"

     [ -z "$trackname" ] && exit
     mpc play "$trackname" || dunstify "Failed to play track" "$checkplayer"
     exit
}

fn_help () {
    printf "ameco : adaptive play/pause toggle for mpd and mpv
Available options (\$1):

help        show this help menu

toggle      main function: toggle play/pause
next        go to next track
prev        go to previous track

forth       seek forwards (+5 sec default)
back        seek backwards (-5 sec default)

start       start mpd and sb modules (use in .xinitrc for dwm)

player      open a terminal window with ncmpcpp
pick        pick a song from current library
pause       pause all mpv instances and mpd
switch      playback options (repeat/single/shuffle)

ameco can be play videos with mpv arguments. Example:
ameco /home/user/lukesmith.mp4 --pause --start=+50

IT IS RECOMMENDED THAT YOU BIND AT LEAST
TOGGLE / NEXT / PREV TO KEYBOARD\n\n"
}

fn_play () {
    case $sptpid in
        "" )
        case $mpdpid in

            "" ) get_target_name
                revol
                mpv $mpvarg $target
                refresh_mpv_module
                getvol
                refresh_block
                ;;

            # watch means utube url. if utube then get name, else use basename
            * )

                [ "$mpdstate" = "playing" ] && resume="yes" || resume="no"
                mpc pause
                get_target_name
                getvol
                mpv $mpvarg $target
                refresh_mpv_module
                revol_mpd
                refresh_block
                # save volume, pause mpd, play item, restore volume then resume after mpv closes

                [ "$resume" = "yes" ] && mpc play ;;
        esac ;;
        * )
            [ "$sptstate" = "Playing" ] && resume="yes" || resume="no"
                sptpause
                get_target_name
                getvol
                mpv $mpvarg $target
                refresh_mpv_module
                revol
                refresh_block
                # save volume, pause mpd, play item, restore volume then resume after mpv closes

                [ "$resume" = "yes" ] && sptplay
    esac
}

##################################################################################################

# SCRIPT STARTS HERE
# this has to be a case statement, else mpd will keep on playing

case $1 in
    "" ) fn_help ;;
    "help" ) fn_help ;;
    "toggle" ) fn_toggle ;;
    "next" ) fn_next ;;
    "prev" ) fn_prev ;;
    "forth" ) fn_seek_fw ;;
    "back" ) fn_seek_bw ;;
    "start" ) fn_start ;; # "ameco start &" in ~/.xinitrc
    "spot" ) fn_start_spot ;; # "ameco spot &" in ~/.xinitrc
    "pause" ) fn_allpause ;;
    "player" ) fn_player ;;
    "select" ) fn_select ;;
    "switch" ) fn_switch ;;
    "only" )
        case $2 in
            video )
                toggle () { mpvtoggle ;}
                next () { mpvnext ;}
                prev () { mpvprev ;}
                forward () { mpvfwd ;}
                rewind () { mpvrwd ;}
                ;;
            music )
                toggle () { mpc toggle ;}
                next () { mpc next ;}
                prev () { mpc prev ;}
                forward () { mpc seek +$seekvalue ;}
                rewind () { mpc seek -$seekvalue ;}
        esac

        case "$3" in
            toggle | next | prev | forward | rewind ) "$3" ;;
            * ) dunstify "Switch not found"
        esac

        ;;
    * ) target="$1"
        while test $# -gt 1
        do
            shift
            case "$@" in --* ) mpvarg="$@"; break ;; esac
            # not sorry for 1 line case statement
        done
        fn_play ;;
esac
