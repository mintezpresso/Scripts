#!/bin/sh
# youtube/invidious module for searchmenu
# mintezpresso

. $HOME/.config/searchmenu/config
. $HOME/.local/bookmarks/yt/config

fn_clipboard () {
    [ -z "$xclip -se c -o" ] && dunstify "Clipboard empty" "Exiting" && exit 1
    case "$(xclip -se c -o)" in

        UC* ) $BROWSER $corelink/channel/"$(xclip -se c -o)" && exit ;;

        * ) clipact=$(printf "Search\nOpen\nAdd to bookmark\nAdd to newsboat\nAdd to bookmark and newsboat" | dmenu -i -p "What to do with input \"$(xclip -se c -o)\" ?")

            [ -z "$clipact" ] && exit 1

            case "$clipact" in

                Search ) $BROWSER $searchlink"$(xclip -se c -o | tr ' ' '+')" && exit ;;

                Open )
                    [ "$defaultengine" = "ytfzf" ] && ytfzf -D "$(xclip -se c -o)" || $BROWSER "$(xclip -se c -o | tr ' ' '+')" ;;

                "Add to bookmark" )
                    entryname=$(dmenu -i -p "Enter a name for the new bookmark" < /dev/null)
                    [ -z "$entryname" ] && exit

                    case $(xclip -se c -o) in
                        https* ) channel_id=$(curl -s $(xclip -se c -o) | grep -woe browseId.*\, | cut -c 12-35 | sed -n 1p) ;;
                        * ) channel_id=$(basename $(xclip -se c -o))
                    esac
                    printf "$entryname - $channel_id\n" >> $bookmark

                    clickme=$(dunstify -t 5000 --action="default,Reply" "\"$entryname\" added to YT bookmark list" "Click to see all bookmarks")
                    [ "$clickme" = "default" ] && $editor $bookmark 2>/dev/null
                    exit
                ;;

                "Add to newsboat" )
                    entryname=$(dmenu -i -p "Enter a name for newsboat bookmark" < /dev/null)
                    [ -z "$entryname" ] && exit

                    case $(xclip -se c -o) in
                        https* ) channel_id=$(curl -s $(xclip -se c -o) | grep -woe browseId.*\, | cut -c 12-35 | sed -n 1p) ;;
                        * ) channel_id=$(basename $(xclip -se c -o))
                    esac

                    . $HOME/.local/bookmarks/yt/rss_config
                    printf "$rss_entry_format\n" >> $newsboat_urls

                    clickme=$(dunstify -t 5000 --action="default,Reply" "\"$entryname\" added to newsboat urls list" "Click to see all urls")
                    [ "$clickme" = "default" ] && $editor $newsboat_urls 2>/dev/null
                    exit
                ;;

                "Add to bookmark and newsboat" )
                    case $(xclip -se c -o) in
                        https* ) channel_id=$(curl -s $(xclip -se c -o) | grep -woe browseId.*\, | cut -c 12-35 | sed -n 1p) ;;
                        * ) channel_id=$(basename $(xclip -se c -o))
                    esac

                    . $HOME/.local/bookmarks/yt/rss_config
                    printf "$entryname - $channel_id\n" >> $bookmark
                    printf "$rss_entry_format\n" >> $newsboat_urls

                    clickme=$(dunstify -t 5000 --action="default,Reply" "\"$entryname\" added to newsboat urls and bookmark" "Click to view")
                    [ "$clickme" = "default" ] && $editor $newsboat_urls 2>/dev/null & $editor $bookmark 2>/dev/null
                    exit

            esac
    esac
}

fn_extra () {
    get_query="$(printf "Clipboard" | dmenu -i -p "Search with $promptalt")"
    [ -z "$get_query" ] && exit

    case $get_query in
        "Clipboard" ) clip_search=$(printf "Yes\nNo" | dmenu -i -p "Search \"$(xclip -se c -o)\" in $defaultengine ?")
        [ -z "$clip_search" ] && exit
        [ "$clip_search" = "Yes" ] && $BROWSER $altlink"$(xclip -se c -o | tr ' ' '+')" ;;
    * ) $BROWSER $altlink"$(printf "$get_query" | tr ' ' '+')" ;;
    esac
}

fn_bookmark () {
    noawk="$(cut -d ' ' -f 1 $bookmark | dmenu -i -p "Select a bookmark")"
    [ -n "$noawk" ] && channelchoice=$(grep -w "^$noawk" $bookmark | cut -d ' ' -f 3) || exit
    [ -n "$channelchoice" ] && $BROWSER $corelink/channel/$channelchoice 2>/dev/null \
        || dunstify "\"$noawk\" not found" "Add new bookmark in \n<b>$bookmark</b>"
    exit
}

case "$defaultengine" in
    invidious ) corelink="$invidiousmirror" && searchlink="$invidiousmirror/search?q=" && prompt="Invidious 📺" \
        && promptalt="Youtube 👿" && altlink="https://www.youtube.com/results?search_query=" \
        && set -- "Clipboard" "ytfzf" "Youtube" "Bookmarks" "Homepage" "Edit" ;;

    youtube ) corelink="https://youtube.com" && searchlink="https://www.youtube.com/results?search_query=" && prompt="Youtube 👿" \
        && promptalt="Invidious 📺" && altlink="$invidiousmirror/search?q=" \
        && set -- "Clipboard" "ytfzf" "Invidious" "Bookmarks" "Homepage" "Edit" ;;

    ytfzf ) set -- "Clipboard" "Invidious" "YouTube" "Edit" && prompt="ytfzf 💫" ;;
esac

ytchoice=$(printf '%s\n' "$@" | dmenu -i -p "Type to search $prompt")
[ -z "$ytchoice" ] && exit
case "$ytchoice" in
    Clipboard ) fn_clipboard ;;
    Invidious | Youtube ) fn_extra ;;
    ytfzf ) [ "$defaultengine" = "ytfzf" ] && ytfzf -D "ytfzf" || ytfzf -D ;;
    Homepage ) $BROWSER $corelink ;;
    Bookmarks ) fn_bookmark ;;
    Edit ) $editor $bookmark 2>/dev/null && exit ;;
    * ) [ "$defaultengine" = "ytfzf" ] && ytfzf -D "$ytchoice" || $BROWSER $searchlink"$(printf "$ytchoice" | tr ' ' '+')" ;;
esac
